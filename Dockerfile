FROM python:3.6-alpine

RUN adduser -D ubuntu

WORKDIR ~/soundcloud-docker/

COPY requirements.txt requirements.txt
COPY soundcloud soundcloud
COPY migrations migrations
RUN apk add --no-cache python3-dev \
     && pip3 install --upgrade pip
RUN apk --no-cache add python3 \
                       build-base \
                       python3-dev \
                       # wget dependency
                       openssl \
                       # dev dependencies
                       git \
                       bash \
                       sudo \
                       py3-pip \
                       # Pillow dependencies
                       jpeg-dev \
                       zlib-dev \
                       freetype-dev \
                       lcms2-dev \
                       openjpeg-dev \
                       tiff-dev \
                       tk-dev \
                       tcl-dev \
                       harfbuzz-dev \
                       fribidi-dev
RUN apk update && apk add gcc libc-dev make git libffi-dev openssl-dev python3-dev libxml2-dev libxslt-dev 
RUN apk update  && apk add postgresql-dev gcc python3-dev musl-dev \
    && pip install psycopg2
RUN python -m venv venv
RUN venv/bin/pip install -r requirements.txt
RUN venv/bin/pip install gunicorn pymysql docker-compose


COPY run.py boot.sh ./
RUN chmod a+x boot.sh

ENV FLASK_APP run.py

USER ubuntu

EXPOSE 5000
ENTRYPOINT ["./boot.sh"]